const data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	}
];

let html = "";

const alpha = data.map(x=>x).sort((a,b)=>a.name>b.name);
const croiss = data.map(x=>x).sort((a,b)=>if(a.price_small===b.price_small){
    return a.price_large>b.price_large;
}else{
    a.price_small>b.price_small;
}
data.map(x => x.name.toLowerCase());

for(let i = 0; i < data.length; i++){
    const pizza = croiss[i];
    const name = pizza.name;
    const url = `images/${name.toLowerCase()}.jpg`;
    html += `<article class="pizzaThumbnail"><a href="${url}">
    <img src="${url}"/>
    <section><h4>${name}</h4>
    <ul>
    <li>Prix petit format : ${pizza.price_small} €</li>
    <li>Prix grand format : ${pizza.price_large} €</li>
    </ul>
    </section>
    </a>
    </article>`;
}
console.log(html);
document.querySelector('.pageContent').innerHTML = html;