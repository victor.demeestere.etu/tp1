"use strict";

var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}];
var html = "";
var alpha = data.map(function (x) {
  return x;
}).sort(function (a, b) {
  return a.name > b.name;
});
var croiss = data.map(function (x) {
  return x;
}).sort(function (a, b) {
  return a.price_small > b.price_small;
});
data.map(function (x) {
  return x.name.toLowerCase();
});

for (var i = 0; i < data.length; i++) {
  var pizza = croiss[i];
  var name = pizza.name;
  var url = "images/".concat(name.toLowerCase(), ".jpg");
  html += "<article class=\"pizzaThumbnail\"><a href=\"".concat(url, "\">\n    <img src=\"").concat(url, "\"/>\n    <section><h4>").concat(name, "</h4>\n    <ul>\n    <li>Prix petit format : ").concat(pizza.price_small, " \u20AC</li>\n    <li>Prix grand format : ").concat(pizza.price_large, " \u20AC</li>\n    </ul>\n    </section>\n    </a>\n    </article>");
}

console.log(html);
document.querySelector('.pageContent').innerHTML = html;
//# sourceMappingURL=main.js.map